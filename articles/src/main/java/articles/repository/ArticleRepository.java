package articles.repository;

import org.springframework.stereotype.Repository;
import articles.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ArticleRepository extends JpaRepository<Article,Long> {
    Article findArticleById(Long id);
}
