package articles.controllers;

import articles.models.Article;
import articles.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ArticlesController {
    @Autowired
    ArticleRepository articleRepository;

    //get all notes
    @GetMapping("/articles")
    public List<Article> getAllArticles(){
        return articleRepository.findAll();
    }

    @PostMapping("/articles")
    public Article createArticle(@RequestBody Article article){
        System.out.println(article.getPrice());
        System.out.println(article.getCategory());
        System.out.println(article.getName());
        System.out.println("----------------ooo");
        System.out.println("----------------ooo");
        System.out.println("----------------ooo");
        System.out.println("----------------ooo");
        System.out.println("----------------ooo");
        System.out.println("----------------ooo");System.out.println("----------------ooo");
        System.out.println("----------------ooo");
        System.out.println("----------------ooo");
        System.out.println("----------------ooo");

        return articleRepository.save(article);
    }


    @GetMapping("/articles/{id}")
    public Article getArticleById(@PathVariable(value ="id") Long articleId){
        return articleRepository.findArticleById((articleId));

    }

    @PutMapping("/articles/{id}")
    public Article updateArticle(@PathVariable(value ="id") Long articleid, @Valid @RequestBody Article article){
        Article articleupdate = articleRepository.findArticleById((articleid));
        if(article != null){
            articleupdate.setCategory(article.getCategory());
            articleupdate.setPrice((article.getPrice()));
            //articleupdate.setExpiration_date(article.getExpiration_date());
            articleupdate.setName(article.getName());
            return articleRepository.save(articleupdate);

        }
        else{
            return null;
        }
    }

    @DeleteMapping("/articles/{id}")
    public ResponseEntity<Article> deleteArticle(@PathVariable(value ="id") Long id){
        Article art = articleRepository.findArticleById(id);
        articleRepository.delete(art);

        return ResponseEntity.ok().build();
    }
}
