package articles.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name ="articles")
public class Article implements Serializable{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="product_id", unique=true, nullable=false)
    private Long id;

    @Column(name="product_price",nullable = false)
    private int price;

    @Column(name="product_category",nullable = false)
    private String category;

    @Column(name="expiration_Date")
    @Temporal(TemporalType.DATE)
    private Date expiration_date;

    @Column(name="name_product",nullable = false)
    private String name;

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }
    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCategory(){
        return this.category;
    }

    public void setCategory(String category){
        this.category = category;
    }

    public Date getExpiration_date(){
        return this.expiration_date;
    }

    public void setExpiration_date(Date date){
        this.expiration_date = date;
    }

    public int getPrice(){
        return this.price;
    }

    public void setPrice(int price){
        this.price = price;
    }

}
